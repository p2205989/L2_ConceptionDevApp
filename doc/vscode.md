# VSCode et C++

VSCode devient l'outil multi-plateforme et multilangage de référence. Regardez les consignes d'[installation ici](./install.md). Pour Linux et MacOS, c'est assez simple. Pour Windows, il est possible d'installer MinGW ou le compilateur de Visual Studio, ne mélanger pas et désinstaller CodeBlocks qui donne une ancienne version de MinGW.

Il y a également la doc de VSCode pour [le C++](https://code.visualstudio.com/docs/languages/cpp). Il faut installer des extensions, mais attention de ne pas installer toutes sortes d'extensions qui seraient en conflit les unes avec les autres. Dans Visual Studio Code, accédez à l'onglet des extensions (icône carrée sur la barre latérale gauche).
* [L'extension CPP de Microsoft](https://code.visualstudio.com/docs/cpp/)
* L'extension `C/C++ IntelliSense, debugging, and code browsing` de Microsoft
* L'extension CMake Tools (version Microsoft).  Recherchez "CMake Tools" et sélectionnez celle proposée par Microsoft.

## Projets

Ne regardez pas les `task.json` mais plutôt tournez vous vers l'utilisation de CMake [en regardant la documentation ici](https://learn.microsoft.com/fr-fr/cpp/build/cmake-projects-in-visual-studio?view=msvc-170).


## Debug

[Regardez les explications de la doc de VSCode](https://code.visualstudio.com/docs/cpp/cpp-debug)


## Git 

[Regardez les explications de la doc de VSCode](https://code.visualstudio.com/docs/sourcecontrol/intro-to-git)
