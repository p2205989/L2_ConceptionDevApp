
# Visual Studio

Si vous êtes sous Windows, installer et utiliser Visual Studio demandera moins de configuration que VSCode et ses extensions, mais il est un peu plus lourd. Visual Studio est un outils tout intégré (compilateur, éditeur, copilot, etc.).

* [Visual Studio](https://visualstudio.microsoft.com/fr/)


Générez votre projet avec CMake et ouvrez le fichier `.vcprj`. Pour la prise en main, il faut pratiquer, notamment autour du débogueur.
